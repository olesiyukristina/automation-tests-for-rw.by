package Tests;

import Pages.MainPage;
import Pages.WebdriverSettings;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.List;

public class SecondTask extends WebdriverSettings {
    MainPage mainPage;
    public static final String COPYRIGHT_TEXT="© 2021 Belarusian Railway";
    public static final int LINKS_COUNT=4;
    public static final List<String> EXPECTED_BUTTONS=Arrays.asList("CONTACTS", "PASSENGER SERVICES", "TICKETS","FREIGHT","CORPORATE","PRESS CENTER");
    @BeforeClass
    public void setUp(){
        mainPage = new MainPage();
    }
    @Test
    public void mainPage_Elements_ShouldDisplayed() {
        mainPage.open();
        mainPage.changeLanguage();
        MatcherAssert.assertThat("not less than 4 news", mainPage.getNewsCount()>=LINKS_COUNT);
        MatcherAssert.assertThat("copyright contains", mainPage.checkCopyright().contains(COPYRIGHT_TEXT));
        Assert.assertEquals(mainPage.checkTopButtons(),EXPECTED_BUTTONS);
    }

}