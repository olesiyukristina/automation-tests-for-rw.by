package Tests;
import Pages.MainPage;
import Pages.SearchResultsPage;
import Pages.WebdriverSettings;
import org.hamcrest.MatcherAssert;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ThirdTask extends WebdriverSettings {
    MainPage mainPage;
    SearchResultsPage searchResultsPage;
    public static final String EXPECTED_TEXT="К сожалению, на ваш поисковый запрос ничего не найдено.";
    public static final String INPUT_TEXT="Санкт-Петербург";
    public static final int NEWS_COUNT=15;
    @BeforeClass
    public void setUp(){
        mainPage=new MainPage();
        searchResultsPage=new SearchResultsPage();
    }
    @Test
     public void checkSearchResults() {
        mainPage.open();
        MatcherAssert.assertThat("address changed", mainPage.getUrl().contains(mainPage.randomInput()));
        Assert.assertEquals(searchResultsPage.checkNoteText(),EXPECTED_TEXT);
        searchResultsPage.typeSearchField(INPUT_TEXT);
        searchResultsPage.getSearchResult();
        Assert.assertEquals(searchResultsPage.getSizeSearchResult(),NEWS_COUNT);
    }
}
