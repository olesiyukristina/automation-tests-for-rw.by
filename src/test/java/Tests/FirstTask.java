package Tests;
import Pages.GooglePage;
import Pages.GooleResultsPage;
import Pages.MainPage;
import Pages.WebdriverSettings;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FirstTask extends WebdriverSettings {
    GooglePage googlePage;
    GooleResultsPage gooleResultsPage;
    MainPage mainPage;
    public static final String INPUT_TEXT="белорусская железная дорога";

    @BeforeClass
    public void setUp(){
        googlePage = new GooglePage();
        gooleResultsPage=new GooleResultsPage();
        mainPage =new MainPage();
    }
    @Test
    public void siteRwBy_mainPage_ShouldLoadFine () {
        googlePage.open();
        googlePage.typeText(INPUT_TEXT);
        googlePage.getSearchResults();
        gooleResultsPage.openSiteRwBy();
        mainPage.waitLoadSite();
    }
}


