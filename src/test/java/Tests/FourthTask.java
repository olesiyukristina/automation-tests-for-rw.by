package Tests;
import Pages.MainPage;
import Pages.SchedulePage;
import Pages.TrainPage;
import Pages.WebdriverSettings;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FourthTask extends WebdriverSettings {
    MainPage mainPage;
    SchedulePage schedulePage;
    TrainPage trainPage;
    public static final String FROM="Брест-Центральный";
    public static final String TO="Минск-Пассажирский";
    public static final String TRAIN_NAME="702Б, Брест-Центральный — Минск-Пассажирский";
    @BeforeClass
    public void setUp(){
        mainPage = new MainPage();
        schedulePage =new SchedulePage();
        trainPage=new TrainPage();
    }

    @Test
    public void checkTrainSchedule(){
        mainPage.open();
        mainPage.typeFromField(FROM);
        mainPage.typeToField(TO);
        mainPage.typeDateField();
        mainPage.findSchedule();
        schedulePage.getTextSchedule();
        schedulePage.openTrainRoute();
        Assert.assertEquals(trainPage.getTrainName(),TRAIN_NAME);
        trainPage.clickSiteLogo();
        mainPage.waitLoadSite();
    }
}
