package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import read.ConfProperties;


public class WebdriverSettings {
    public static WebDriver driver;
    public static WebDriverWait wait;

    public WebdriverSettings() {
    }

    @BeforeClass
    public WebDriver PreCondition(){
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        driver=new ChromeDriver();
        wait=new WebDriverWait(driver,30);
        return driver;
    }

    @AfterClass
    public void afterTest(){
        driver.close();

    }
}
