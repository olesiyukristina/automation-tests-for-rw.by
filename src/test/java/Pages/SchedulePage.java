package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SchedulePage extends BasePage {
    public SchedulePage(WebDriver driver) {
        super(driver);
    }

    @FindBy (xpath = "//div[@id=\"sch-route\"]//div[@class=\"sch-table__body js-sort-body\"]")
    private WebElement scheduleResults;
    By scheduleLocator=By.className("sch-table__row_2");
    private List<WebElement> getSchedule;

    @FindBy (className ="train-route" )
    private WebElement trainRoute;

    public SchedulePage() {
        super(driver);
    }

    public void getTextSchedule(){
        getSchedule=scheduleResults.findElements(scheduleLocator);
        for (int i = 0; i < getSchedule.size(); i++)
            System.out.println(getSchedule.get(i).getText());
    }
    public void openTrainRoute(){
        trainRoute.click();

    }

}
