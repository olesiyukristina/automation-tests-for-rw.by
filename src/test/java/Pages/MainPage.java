package Pages;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MainPage extends BasePage  {
   
    @FindBy(css = "[href='/en/']")
    private WebElement en;

    @FindBy(css = ".index-news-list-wrap")
    private WebElement news;
    By newsLocator= By.className("news-date-intend");
    private List<WebElement> newsList;
    @FindBy (className ="copyright")
    private WebElement getCopyright;
    @FindBy(id ="main_menu")
    private WebElement mainMenu;
    By buttonsLocator=By.tagName("b");
    private List<WebElement> getButtons;
    @FindBy(id = "searchinp")
    private WebElement search;
    @FindBy(id = "acFrom")
    private WebElement fromField;
    @FindBy(id = "acTo")
    private WebElement toField;
    @FindBy(id = "yDate")
    private WebElement dateField;
    @FindBy(className = "button-wrapper")
    private WebElement find;

    public MainPage(WebDriver driver) {super(driver);
    }

    public MainPage() {
        super(driver);
    }


    public void waitLoadSite(){
        wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));
    }
    public void open(){
        driver.get("https://www.rw.by/");
    }
    public void changeLanguage(){
        en.click();
    }
    public int getNewsCount() {
        newsList = news.findElements(newsLocator);
        return newsList.size();
    }
    public String checkCopyright(){
        return getCopyright.getText();
    }
    public ArrayList<String> checkTopButtons(){
        getButtons=mainMenu.findElements(buttonsLocator);
        ArrayList<String> actualButtons=new ArrayList<>();
        for (WebElement elem: getButtons)
        {actualButtons.add(elem.getText());}
        return actualButtons;
    }
    public String randomInput(){
        String random= RandomStringUtils.randomAlphanumeric(20);
        search.sendKeys(random, Keys.ENTER);
        return search.getText();

    }
    public String getUrl(){
        String myText=driver.getCurrentUrl();
        return myText;
    }
    public void typeFromField (String from){
        fromField.sendKeys(from);
}
    public void typeToField(String to){
        toField.sendKeys(to);
    }
    public void typeDateField(){
        LocalDate date=LocalDate.now().plusDays(5);
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String formattedString=date.format(formatter);
        dateField.sendKeys(formattedString, Keys.ENTER);
    }
    public void findSchedule(){
        find.click();
    }

}




