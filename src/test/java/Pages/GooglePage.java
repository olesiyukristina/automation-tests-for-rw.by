package Pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GooglePage extends BasePage
{
    @FindBy(xpath = "//input[@aria-label='Найти']")
    private WebElement inputSearch;
    @FindBy(name = "btnK")
    private WebElement searchButton;

    public GooglePage() {
        super(driver);
    }


    public void open(){
        driver.get("https://www.google.com/");
    }
    public void typeText (String searchInput){

        inputSearch.sendKeys(searchInput);
    }
    public void getSearchResults(){
        wait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
    }

    }


