package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class TrainPage extends BasePage {

    public TrainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class=\"sch-title\"]//div[@class=\"sch-title__title h2\"]")
            private WebElement trainName;
    @FindBy(className ="sch-title__descr" )
    private WebElement routeDays;
@FindBy(className = "logo-png")
private WebElement siteLogo;

    public TrainPage() {
        super(driver);
    }


    public String getTrainName(){
        return trainName.getText();
    }
    public String getRouteStatus(){
        return routeDays.getText();
    }
public void clickSiteLogo(){
        siteLogo.click();
}
}
