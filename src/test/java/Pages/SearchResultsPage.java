package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchResultsPage extends BasePage {
    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(className = "notetext")
    private WebElement noteText;
    @FindBy(id = "searchinpm")
    private WebElement search;
    @FindBy(className = "search-button")
    private WebElement searchButtonLocator;
    @FindBy(css = "div.search-result")
    private WebElement searchResultLocator;
    By linksLocator=By.className("name");
    private List<WebElement> getLinks;

    public SearchResultsPage() {
        super(driver);
    }


    public String checkNoteText(){
        String actualText=noteText.getText();
        return actualText;
    }
    public void typeSearchField(String searchInput){
        search.click();
        search.clear();
        search.sendKeys(searchInput);
    }
    public void getSearchResult(){
        searchButtonLocator.click();
    }

    public int getSizeSearchResult(){
        getLinks=searchResultLocator.findElements(linksLocator);
        for (int i = 0; i < getLinks.size(); i++)
            System.out.println(getLinks.get(i).getText());
        return getLinks.size();
}

}
