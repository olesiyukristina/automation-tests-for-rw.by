package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GooleResultsPage extends BasePage {

    public GooleResultsPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//cite[@class=\"iUh30 Zu0yb tjvcx\"]")
    private WebElement url;

    public GooleResultsPage() {
        super(driver);
    }

    public void openSiteRwBy() {
        url.click();
    }

}